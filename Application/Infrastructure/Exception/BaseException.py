import falcon
import json

try:
    from collections import OrderedDict
except ImportError:
    OrderedDict = dict

ERR_UNKNOWN = {
    'status': falcon.HTTP_500,
    'code': 500,
    'title': 'Unknown Error'
}


class AppError(Exception):
    def __init__(self, error=ERR_UNKNOWN, description=None) -> None:  # noqa
        '''

        :param error:
        :type error:
        :param description:
        :type description:
        '''
        self.error = error
        self.error['description'] = description

    @property
    def code(self) -> str:
        '''

        :return:
        :rtype:
        '''
        return self.error['code']

    @property
    def title(self) -> str:
        '''

        :return:
        :rtype:
        '''
        return self.error['title']

    @property
    def status(self) -> str:
        '''

        :return:
        :rtype:
        '''
        return self.error['status']

    @property
    def description(self) -> str:
        '''

        :return:
        :rtype:
        '''
        return self.error['description']

    @staticmethod
    def handle(exception, req, res, error=None) -> json:
        '''

        :param exception:
        :type exception:
        :param req:
        :type req:
        :param res:
        :type res:
        :param error:
        :type error:
        :return:
        :rtype:
        '''
        res.status = exception.status
        meta = OrderedDict()
        meta['code'] = exception.code
        meta['message'] = exception.title
        if exception.description:
            meta['description'] = exception.description
        res.body = json.dumps({'meta': meta})


class SocketError(Exception):
    def __init__(self, error=ERR_UNKNOWN, description=None) -> None:
        '''

        :param error:
        :type error:
        :param description:
        :type description:
        '''
        self.error = error
        self.error['description'] = description
