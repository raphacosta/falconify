import falcon
from .BaseException import AppError

try:
    from collections import OrderedDict
except ImportError:
    OrderedDict = dict

OK = {
    'status': falcon.HTTP_200,
    'code': 200,
}

ERR_NOT_FOUND = {
    'status': falcon.HTTP_404,
    'code': 404,
    'title': 'No Result Found'
}

ERR_AUTH_REQUIRED = {
    'status': falcon.HTTP_401,
    'code': 99,
    'title': 'Authentication Required'
}

ERR_UNSUP_MEDIA_TYPE = {
    'status': falcon.HTTP_415,
    'code': 415,
    'title': 'Unsupported Media Type'
}

ERR_INVALID_PARAMETER = {
    'status': falcon.HTTP_400,
    'code': 88,
    'title': 'Invalid Parameter'
}

ERR_INVALID_STREAM = {
    'status': falcon.HTTP_400,
    'code': 88,
    'title': 'Invalid Stream'
}

ERR_RES_EMPTY = {
    'status': falcon.HTTP_404,
    'code': 404,
    'title': 'No Results Found'
}

ERR_DATABASE_ROLLBACK = {
    'status': falcon.HTTP_500,
    'code': 77,
    'title': 'Database Rollback Error'
}

ERR_GENERAL_EXCEPTION = {
    'status': falcon.HTTP_400,
    'code': 11,
    'title': 'Bad Request'
}

ERR_NOT_SUPPORTED = {
    'status': falcon.HTTP_404,
    'code': 10,
    'title': 'Not Supported'
}

ERR_CONFLICT = {
    'status': falcon.HTTP_409,
    'code': 409,
    'title': 'Conflict'
}


class DatabaseError(AppError):
    def __init__(self, error, args=None, params=None) -> None:  # noqa
        '''

        :param error:
        :type error:
        :param args:
        :type args:
        :param params:
        :type params:
        '''
        super().__init__(error)
        obj = OrderedDict()
        obj['details'] = ', '.join(args)
        obj['params'] = str(params)
        self.error['description'] = obj


class SchemaValidationError(AppError):
    def __init__(self, description=None) -> None:
        '''

        :param description:
        :type description:
        '''
        super().__init__(ERR_INVALID_PARAMETER)
        self.error['description'] = description


class InvalidStreamError(AppError):
    def __init__(self, description=None) -> None:
        '''

        :param description:
        :type description:
        '''
        super().__init__(ERR_INVALID_PARAMETER)
        self.error['description'] = description


class InvalidParameterError(AppError):
    def __init__(self, description=None) -> None:
        '''

        :param description:
        :type description:
        '''
        super().__init__(ERR_INVALID_PARAMETER)
        self.error['description'] = description


class UnauthorizedError(AppError):
    def __init__(self, description=None):
        super().__init__(ERR_AUTH_REQUIRED)
        self.error['description'] = description


class GeneralExceptionError(AppError):
    def __init__(self, description=None) -> None:
        '''

        :param description:
        :type description:
        '''
        super().__init__(ERR_GENERAL_EXCEPTION)
        self.error['description'] = description


class UnsupportedMediaType(AppError):
    def __init__(self, description=None) -> None:
        '''

        :param description:
        :type description:
        '''
        super().__init__(ERR_UNSUP_MEDIA_TYPE)
        self.error['description'] = description


class GeneralNotFound(AppError):
    def __init__(self, description=None) -> None:
        '''

        :param description:
        :type description:
        '''
        super().__init__(ERR_NOT_FOUND)
        self.error['description'] = description


class NotSupportedError(AppError):
    def __init__(self, method=None, url=None) -> None:
        '''

        :param method:
        :type method:
        :param url:
        :type url:
        '''
        super().__init__(ERR_NOT_SUPPORTED)
        if method and url:
            self.error['description'] = 'method: %s, url: %s' % (method, url)


class ConflictError(AppError):
    def __init__(self, description=None) -> None:
        '''

        :param description:
        :type description:
        '''
        super().__init__(ERR_CONFLICT, description)


class NoResultsFoundError(AppError):
    def __init__(self, description=None) -> None:
        '''

        :param description:
        :type description:
        '''
        super().__init__(ERR_RES_EMPTY, description)
