import datetime


class AccessTransformer:
    def __init__(self):
        pass

    def transform(self, data, method, type):
        """

        :param data:
        :type data:
        :param method:
        :type method:
        :param type:
        :type type:
        :return:
        :rtype:
        """
        if method == 'POST':
            return self._fulfill_post_contract(data, type)
        else:
            return data

    def _fulfill_post_contract(self, data, type='grant'):
        """

        :param data:
        :type data:
        :param type:
        :type type:
        :return:
        :rtype:
        """
        if type in ['grant', 'reset']:
            return {
                "user": {
                    "uuid": str(data['user'].uuid),
                    "first_name": data['user'].first_name,
                    "last_name": data['user'].last_name,
                    "username": data['user'].username,
                    "email": data['user'].email,
                    "info": data['user'].info,
                },
                "role": {
                    "name": data['user'].user_role.role.name,
                    "shortname": data['user'].user_role.role.shortname,
                    "is_admin": data['user'].user_role.role.is_admin,
                    "matrix": data['user'].user_role.role.matrix,
                },
                "auth": {
                    "token": str(data['token_set']['token']),
                    "refresh_token": str(data['token_set']['refresh_token']),
                    "issued": str(datetime.datetime.now())
                }
            }

        else:
            pass
