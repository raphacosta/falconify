class BaseTransformer:

    def __init__(self):
        pass

    def not_found(self):
        return {
            "code": 404,
            "description": "NOT FOUND"
        }
