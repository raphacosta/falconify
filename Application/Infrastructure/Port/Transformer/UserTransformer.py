class UserTransformer:
    def __init__(self):
        pass

    def transform(self, data, method):
        """

        :param data:
        :type data:
        :param method:
        :type method:
        :return:
        :rtype:
        """
        if method == 'POST':
            return self._fulfill_post_contract(data)
        elif method == 'GET':
            return self._fulfill_get_contract(data)
        elif method == 'PUT':
            return self._fulfill_put_contract(data)
        else:
            return data

    def _fulfill_post_contract(self, data):
        """

        :param data:
        :type data:
        :return:
        :rtype:
        """
        return {
            'uuid': str(data)
        }

    def _fulfill_get_contract(self, data):
        """

        :param data:
        :type data:
        :return:
        :rtype:
        """
        result = []
        if isinstance(data, list):
            for row in data:
                result.append({
                    "uuid": str(row.uuid),
                    "username": row.username,
                    "first_name": row.first_name,
                    "last_name": row.last_name,
                    "email": row.email,
                    "role": {
                        "uuid": str(row.user_role.role.uuid),
                        "shortname": row.user_role.role.shortname,
                        "name": row.user_role.role.name,
                    },
                    "info": row.info,
                    "last_login": str(row.last_login)
                })
        else:
            row = data
            result.append({
                "uuid": str(row.uuid),
                "username": row.username,
                "first_name": row.first_name,
                "last_name": row.last_name,
                "email": row.email,
                "role": {
                    "uuid": str(row.user_role.role.uuid),
                    "shortname": row.user_role.role.shortname,
                    "name": row.user_role.role.name,
                },
                "info": row.info,
                "last_login": str(row.last_login),
                "is_active": data.is_active,
                "created": str(data.created),
                "modified": str(data.modified)
            })

        return result

    def _fulfill_put_contract(self, data):
        """

        :param data:
        :type data:
        :return:
        :rtype:
        """
        return {
            'uuid': str(data)
        }
