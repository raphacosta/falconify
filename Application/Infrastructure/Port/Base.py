# -*- coding: utf-8 -*-
import falcon
import json
from Adapters.SqlAlchemy.Alchemy import new_alchemy_encoder

try:
    from collections import OrderedDict
except ImportError:
    OrderedDict = dict

from Config.Config import APP_ENV, BRAND_NAME, AUTHOR, AUTHOR_EMAIL
from Application.Infrastructure.Exception.RuntimeException \
    import NotSupportedError


class BaseResource(object):
    HELLO_WORLD = {
        'server': f'Welcome to {BRAND_NAME}',  # noqa
        'author': f'{AUTHOR} <{AUTHOR_EMAIL}>',
        'documentation':
            'Coming Soon',
        'environment': f'{APP_ENV}'
    }

    def to_json(self, body_dict):
        """
        Dumps dict to json

        :param body_dict:
        :type body_dict:
        :return:
        :rtype:
        """
        return json.dumps(body_dict)

    def from_db_to_json(self, db):
        """
        Alchemy encoder

        :param db:
        :type db:
        :return:
        :rtype:
        """
        return json.dumps(db, cls=new_alchemy_encoder())

    def on_error(self, res, error=None):
        """
        Handles error responses

        :param res:
        :type res:
        :param error:
        :type error:
        :return:
        :rtype:
        """
        if error:
            error = error.error

        res.status = error['status']
        meta = OrderedDict()
        meta['code'] = error['code']
        meta['message'] = error['description']

        obj = OrderedDict()
        obj['meta'] = meta
        res.body = self.to_json(obj)

    def on_success(self, res, data=None, count=None, total=None,
                   pagination_meta=None):
        """
        Handle successful response

        :param res:
        :param data:
        :param count:
        :param total:
        :param pagination_meta:
        :return:
        """
        res.status = falcon.HTTP_200
        meta = OrderedDict()

        meta['code'] = 200
        meta['message'] = 'OK'
        if data is not None and count is not None:
            meta['results'] = len(data)
            if total is not None:
                meta['total'] = total

        if pagination_meta is not None:
            meta['pagination'] = pagination_meta

        obj = OrderedDict()
        obj['meta'] = meta
        obj['data'] = data
        res.body = self.to_json(obj)

    def on_get(self, req, res):
        """
        Hello World - Default response for the service

        :param req:
        :type req:
        :param res:
        :type res:
        :return:
        :rtype:
        """
        if req.path == '/':
            res.status = falcon.HTTP_200
            res.body = self.to_json(self.HELLO_WORLD)
        else:
            raise NotSupportedError(method='GET', url=req.path)

    def on_post(self, req, res):
        """
        Call Not Supported

        :param req:
        :type req:
        :param res:
        :type res:
        :return:
        :rtype:
        """
        raise NotSupportedError(method='POST', url=req.path)

    def on_put(self, req, res):
        """
        Call Not Supported

        :param req:
        :type req:
        :param res:
        :type res:
        :return:
        :rtype:
        """
        raise NotSupportedError(method='PUT', url=req.path)

    def on_delete(self, req, res):
        """
        Call Not Supported

        :param req:
        :type req:
        :param res:
        :type res:
        :return:
        :rtype:
        """
        raise NotSupportedError(method='DELETE', url=req.path)
