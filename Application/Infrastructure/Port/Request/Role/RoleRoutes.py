from .RoleRequestHandler import \
    RoleCollectionRequestHandler
from .RoleRequestHandler import \
    RoleItemRequestHandler


class RoleRoutes:

    _version = '/v1/'

    def define(self, route):
        route.add_route(
            self._version + "roles",
            RoleCollectionRequestHandler())

        route.add_route(
            self._version + "roles/{uuid}",
            RoleItemRequestHandler())
