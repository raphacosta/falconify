from Config import Log
from Application.Infrastructure.Port.Base \
    import BaseResource
from Application.Features.Role.RoleCommandHandler \
    import RoleCommandHandler
from Application.Infrastructure.Port.Transformer.RoleTransformer \
    import RoleTransformer


Log = Log.get_logger()


class RoleCollectionRequestHandler(BaseResource):

    def on_get(self, req, res):
        """
        Receives the command to fetch all Roles
        from the collection

        :param req:
        :param res:
        :return:
        """
        try:
            qry = RoleCommandHandler(req, res).list()
            contract_response = RoleTransformer()\
                .transform(qry, req.method)
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)

    def on_post(self, req, res):
        """
        Creates a new entity of a role

        :param req:
        :param res:
        :return:
        """
        try:
            cmd = RoleCommandHandler(req, res).create()
            contract_response = RoleTransformer()\
                .transform(cmd, req.method)
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)


class RoleItemRequestHandler(BaseResource):

    def on_get(self, req, res, uuid):
        """
        List an entity of a role by uuid
        :param req:
        :param res:
        :param uuid: uuid
        :return:
        """
        try:
            cmd = RoleCommandHandler(req, res).load(uuid)
            contract_response = RoleTransformer()\
                .transform(cmd, req.method)
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)

    def on_put(self, req, res, uuid):
        """
        Updates the entity of a role loaded by uuid

        :param req:
        :param res:
        :param uuid:
        :return:
        """
        try:
            cmd = RoleCommandHandler(req, res).update(uuid)
            contract_response = RoleTransformer()\
                .transform(cmd, req.method)
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)
