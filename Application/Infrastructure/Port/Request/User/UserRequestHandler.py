import falcon
from Config import Log
from Application.Infrastructure.Port.Base \
    import BaseResource
from Application.Infrastructure.Middleware.Hooks.Auth \
    import auth_required
from Application.Features.User.UserCommandHandler \
    import UserCommandHandler
from Application.Infrastructure.Port.Transformer.UserTransformer \
    import UserTransformer


Log = Log.get_logger()


class UserCollectionRequestHandler(BaseResource):

    @falcon.before(auth_required)
    def on_get(self, req, res):
        """
        Receives the command to fetch all asset types
        from the collection

        :param req:
        :type req:
        :param res:
        :type res:
        :return:
        :rtype:
        """
        try:
            qry = UserCommandHandler(req, res).list()
            contract_response = UserTransformer()\
                .transform(qry, req.method)
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)

    @falcon.before(auth_required)
    def on_post(self, req, res):
        """

        :param req:
        :type req:
        :param res:
        :type res:
        :return:
        :rtype:
        """
        try:
            cmd = UserCommandHandler(req, res).create()
            contract_response = UserTransformer()\
                .transform(cmd, req.method)
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)


class UserItemRequestHandler(BaseResource):

    @falcon.before(auth_required)
    def on_get(self, req, res, uuid):
        """

        :param req:
        :type req:
        :param res:
        :type res:
        :param uuid:
        :type uuid:
        :return:
        :rtype:
        """
        try:
            cmd = UserCommandHandler(req, res).load(uuid)
            contract_response = UserTransformer()\
                .transform(cmd, req.method)
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)

    @falcon.before(auth_required)
    def on_put(self, req, res, uuid):
        """

        :param req:
        :type req:
        :param res:
        :type res:
        :param uuid:
        :type uuid:
        :return:
        :rtype:
        """
        try:
            cmd = UserCommandHandler(req, res).update(uuid)
            contract_response = UserTransformer()\
                .transform(cmd, req.method)
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)
