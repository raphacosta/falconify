from .UserRequestHandler import \
    UserCollectionRequestHandler
from .UserRequestHandler import \
    UserItemRequestHandler


class UserRoutes:

    _version = '/v1/'

    def define(self, route):
        route.add_route(
            self._version + "users",
            UserCollectionRequestHandler())

        route.add_route(
            self._version + "users/{uuid}",
            UserItemRequestHandler())
