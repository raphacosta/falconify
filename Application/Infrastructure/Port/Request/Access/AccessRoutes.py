from .AccessRequestHandler import \
    AccessCollectionRequestHandler, ResetRequestHandler


class AccessRoutes:

    _version = '/v1/'

    def define(self, route):
        route.add_route(
            self._version + "users/self/login",
            AccessCollectionRequestHandler())

        route.add_route(
            self._version + "users/self/reset",
            ResetRequestHandler())
