from Application.Infrastructure.Port.Base \
    import BaseResource
from Application.Features.Access.AccessCommandHandler \
    import AccessCommandHandler
from Application.Infrastructure.Port.Transformer.AccessTransformer \
    import AccessTransformer
from Application.Infrastructure.Exception.RuntimeException \
    import InvalidParameterError
from cerberus import Validator, ValidationError
from Domain.UseCases.Access import AccessDomain


class AccessCollectionRequestHandler(BaseResource):

    def on_post(self, req, res):
        """

        :param req:
        :type req:
        :param res:
        :type res:
        :return:
        :rtype:
        """
        v = Validator(AccessDomain.retrieve_valid_schema('grant'))
        try:
            if not v.validate(req.context['data']):
                raise InvalidParameterError(v.errors)
        except ValidationError:
            raise InvalidParameterError('Invalid Request %s' % req.context)

        try:
            cmd = AccessCommandHandler(req, res).grant_access()
            contract_response = AccessTransformer()\
                .transform(cmd, req.method, 'grant')
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)


class ResetRequestHandler(BaseResource):

    def on_post(self, req, res):
        """

        :param req:
        :type req:
        :param res:
        :type res:
        :return:
        :rtype:
        """
        v = Validator(AccessDomain.retrieve_valid_schema('reset'))
        try:
            if not v.validate(req.context['data']):
                raise InvalidParameterError(v.errors)
        except ValidationError:
            raise InvalidParameterError('Invalid Request %s' % req.context)

        try:
            cmd = AccessCommandHandler(req, res).reset()
            contract_response = AccessTransformer()\
                .transform(cmd, req.method, 'reset')
            self.on_success(res, contract_response)
        except TypeError as e:
            self.on_error(res, e)
