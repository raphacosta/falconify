# -*- coding: utf-8 -*-
import json
from Application.Infrastructure.Exception.RuntimeException \
    import UnsupportedMediaType, InvalidParameterError


class RequestManager(object):

    def process_request(self, req, res):
        """
        Handle pre-processing of the request (before routing).
        """
        res.set_header('Access-Control-Allow-Origin', '*')
        res.set_header('Access-Control-Allow-Methods', '*')
        res.set_header('Access-Control-Allow-Headers', '*')

        if req.method == "POST" and \
                req.get_header('Content-Type') != "application/json":
            raise UnsupportedMediaType()

        if req.method == "PUT" and \
                req.get_header('Content-Type') != "application/json":
            raise UnsupportedMediaType()

        if req.method == "POST" or req.method == "PUT":
            try:
                raw_json = req.stream.read()
            except Exception:
                message = 'Read Error'
                raise InvalidParameterError(message)
            try:
                req.context['data'] = json.loads(raw_json.decode('utf-8'))
            except ValueError:
                raise InvalidParameterError(
                    'No JSON object could be decoded or Malformed JSON'
                )
            except UnicodeDecodeError:
                raise InvalidParameterError(
                    'Cannot be decoded by utf-8'
                )

    def process_response(self, req, res, resource, req_succeeded=None):
        """
        Handle post-processing of the response (after routing).
        """
        res.set_header('Access-Control-Allow-Origin', '*')
        res.set_header('Access-Control-Allow-Methods', '*')
        res.set_header('Access-Control-Allow-Headers', '*')
