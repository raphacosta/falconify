# -*- coding: utf-8 -*-
from Application.Infrastructure.Exception.RuntimeException \
    import UnauthorizedError


def auth_required(req, res, resource, *args):

    if 'auth_user' not in req.context:
        raise UnauthorizedError()

    if req.context['auth_user'] is None:
        raise UnauthorizedError()
