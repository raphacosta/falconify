# -*- coding: utf-8 -*-
from Domain.UseCases.Access import AccessDomain
from Application.Infrastructure.Exception.RuntimeException \
    import UnauthorizedError


class AuthHandler(object):

    def process_request(self, req, res):

        if req.auth is not None and req.auth != 'null':

            # verifies if the token is valid and active. '''
            token = AccessDomain.decrypt_token(req.auth)

            if token is False:
                raise UnauthorizedError(
                    'Auth Token has expired. Please login again.'
                )

            # follows the flow with token verification '''
            if token is None:
                UnauthorizedError(
                    'Auth Token has expired. Please login again.'
                )
            else:
                req.context['auth_user'] = str(token['public_id'])

                AccessDomain(req.context['session']).validate_role(
                    req.context['auth_user'], req.path, req.method)
        else:
            UnauthorizedError('Auth Token has expired. Please login again.')
