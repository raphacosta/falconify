from sqlalchemy.orm.exc import NoResultFound
from Application.Infrastructure.Port.Transformer.BaseTransformer \
    import BaseTransformer
from Domain.UseCases.Access import AccessDomain


class AccessCommandHandler:

    def __init__(self, req, res):
        self.req = req
        self.res = res
        self.session = req.context['session']

    def grant_access(self):
        """

        :return:
        :rtype:
        """
        try:
            return AccessDomain(self.session)\
                .grant_access(self.req.context['data']['email'],
                              self.req.context['data']['password'])

        except NoResultFound:
            return BaseTransformer().not_found()

    def reset(self):
        """

        :return:
        :rtype:
        """
        try:
            return AccessDomain(self.session)\
                .reset(self.req.context['data']['uuid'],
                       self.req.context['data']['token'],
                       self.req.context['data']['refresh_token'])

        except NoResultFound:
            return BaseTransformer().not_found()
