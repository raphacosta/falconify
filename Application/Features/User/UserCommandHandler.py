from sqlalchemy.orm.exc import NoResultFound
from cerberus import Validator, ValidationError
from Application.Infrastructure.Port.Transformer.BaseTransformer \
    import BaseTransformer
from Application.Infrastructure.Exception.RuntimeException \
    import InvalidParameterError
from Domain.UseCases.User import UsersDomain


class UserCommandHandler:

    def __init__(self, req, res):
        self.req = req
        self.res = res
        self.session = req.context['session']

    def list(self):
        """

        :return:
        :rtype:
        """
        try:
            return UsersDomain(self.session).list_all()
        except NoResultFound:
            return BaseTransformer().not_found()

    def load(self, uuid):
        """

        :param uuid:
        :type uuid:
        :return:
        :rtype:
        """
        try:
            return UsersDomain(self.session).load(uuid)
        except NoResultFound:
            raise NoResultFound()

    def create(self):
        """

        :return:
        :rtype:
        """

        schema = UsersDomain.retrieve_valid_schema()

        # validate payload against schema retrieved from the domain
        v = Validator(schema)

        try:
            if not v.validate(self.req.context['data']):
                raise InvalidParameterError(v.errors)
        except ValidationError:
            raise InvalidParameterError(
                'Invalid Request %s' % self.req.context)

        user = UsersDomain(self.session)\
            .is_email_unique(self.req.context['data']['email'])

        if user > 0:
            raise InvalidParameterError(
                'The email %s already exists.'
                '' % self.req.context['data']['email'])
        else:
            return UsersDomain(self.session).\
                create(self.req.context['data'])

    def update(self, uuid):
        """

        :param uuid:
        :type uuid:
        :return:
        :rtype:
        """
        return \
            UsersDomain(self.session).update(
                uuid, self.req.context['data'])
