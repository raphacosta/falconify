from sqlalchemy.orm.exc import NoResultFound
from cerberus import Validator, ValidationError
from Application.Infrastructure.Port.Transformer.BaseTransformer \
    import BaseTransformer
from Application.Infrastructure.Exception.RuntimeException \
    import InvalidParameterError
from Domain.UseCases.Role import RolesDomain


class RoleCommandHandler:

    def __init__(self, req, res):
        self.req = req
        self.res = res
        self.session = req.context['session']

    def list(self):
        """

        :return:
        :rtype:
        """
        try:
            return RolesDomain(self.session).list()
        except NoResultFound:
            return BaseTransformer().not_found()

    def load(self, uuid):
        """

        :param uuid:
        :type uuid:
        :return:
        :rtype:
        """
        try:
            return RolesDomain(self.session).load(uuid)
        except NoResultFound:
            raise NoResultFound()

    def create(self):
        """

        :return:
        :rtype:
        """

        schema = RolesDomain.retrieve_valid_schema()

        v = Validator(schema)

        try:
            if not v.validate(self.req.context['data']):
                raise InvalidParameterError(v.errors)
        except ValidationError:
            raise InvalidParameterError(
                'Invalid Request %s' % self.req.context)

        uuid = RolesDomain(self.session).\
            create(self.req.context['data'])

        return uuid

    def update(self, uuid):
        """

        :param uuid:
        :type uuid:
        :return:
        :rtype:
        """
        return \
            RolesDomain(self.session).update(
                uuid, self.req.context['data'])
