import falcon
from Config.Config import APP_ENV, SENTRY_DSN
from Config import Log
from Application.Infrastructure.Middleware.RequestManager import \
    RequestManager
from Application.Infrastructure.Middleware.SessionManager import \
    DatabaseSessionManager
from Application.Infrastructure.Middleware.AuthManager import \
    AuthHandler
from Adapters.SqlAlchemy import db_session, init_session
from Application.Infrastructure.Exception.BaseException \
    import AppError
from Application.Infrastructure.Port import Base
from Application.Infrastructure.Port.Request.User.\
    UserRoutes import UserRoutes
from Application.Infrastructure.Port.Request.Access.\
    AccessRoutes import AccessRoutes
from Application.Infrastructure.Port.Request.Role.RoleRoutes\
    import RoleRoutes

Log = Log.get_logger()


if APP_ENV != 'local':
    import sentry_sdk
    sentry_sdk.init(SENTRY_DSN)


class App(falcon.API):

    version = '/v1/'

    # todo: pass this onto module __init__
    def __init__(self, *args, **kwargs):
        super(App, self).__init__(*args, **kwargs)
        Log.info('API server is starting')

        self.add_route('/', Base.BaseResource())

        # setting User routes
        UserRoutes().define(self)

        # setting Access routes
        AccessRoutes().define(self)

        # setting Roles routes
        RoleRoutes().define(self)

        # invoking error handler
        self.add_error_handler(AppError, AppError.handle)


init_session()


middleware = [
    RequestManager(),
    DatabaseSessionManager(db_session),
    AuthHandler()
]


application = App(middleware=middleware)

if __name__ == "__main__":
    from wsgiref import simple_server
    httpd = simple_server.make_server('0.0.0.1', 8000, application)
    httpd.serve_forever()
