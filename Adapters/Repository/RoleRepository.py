from Adapters.SqlAlchemy.Model.Base import Base
from Domain.Model.Role import Role
from sqlalchemy.exc import DataError
from Application.Infrastructure.Exception.RuntimeException \
    import InvalidParameterError
from Application.Infrastructure.Exception.RuntimeException \
    import GeneralExceptionError
import uuid


class RoleRepository(Base):

    def __init__(self, session):
        """

        :param session:
        """
        self.session = session

    def list(self):
        """

        :return:
        """
        try:
            return self.session.query(Role).all()
        except Exception as e:
            raise GeneralExceptionError(
                "{}".format(type(e)))

    def load_by_shortname(self, shortname):
        """

        :param shortname:
        :return:
        """
        try:
            return self.session.query(Role)\
                .filter(Role.shortname == shortname.lower())\
                .limit(1)\
                .first()

        except DataError as e:
            raise InvalidParameterError(str(e.orig))
        except Exception as e:
            raise GeneralExceptionError(
                "{} - for {}".format(type(e), shortname))

    def load_by_uuid(self, uuid):
        """

        :param uuid:
        :return:
        """
        try:
            return self.session.query(Role) \
                .filter(Role.uuid == uuid)\
                .first()

        except DataError as e:
            raise InvalidParameterError(str(e.orig))
        except Exception as e:
            raise GeneralExceptionError(
                "{} - for {}".format(type(e), uuid))

    def load_default(self):
        """

        :return:
        """
        try:
            return self.session.query(Role) \
                .filter(Role.is_default)\
                .first()

        except DataError as e:
            raise InvalidParameterError(str(e.orig))
        except Exception as e:
            raise GeneralExceptionError(
                "{}".format(type(e)))

    def create(self, data):
        """

        :param data:
        :return:
        """
        role = Role()

        try:
            role.uuid = uuid.uuid4()
            role.name = data['name']
            role.shortname = data['shortname']

            if 'is_admin' in data:
                role.is_admin = data['is_admin']

            if 'is_default' in data:
                role.is_admin = data['is_default']

            if 'matrix' in data:
                role.matrix = data['matrix']

            self.session.add(role)
            self.session.commit()

            return role.uuid
        except Exception as e:
            raise GeneralExceptionError(
                "{}".format(type(e)))

    def update(self, uuid, data):
        """

        :param uuid:
        :param data:
        :return:
        """
        role = self.load_by_uuid(uuid)

        try:
            if 'name' in data:
                role.name = data['name']

            if 'shortname' in data:
                role.shortname = data['shortname']

            if 'is_admin' in data:
                role.is_admin = data['is_admin']

            if 'is_default' in data:
                role.is_default = data['is_default']

            if 'matrix' in data:
                role.matrix = data['matrix']

            self.session.commit()

            return role.uuid
        except Exception as e:
            raise GeneralExceptionError(
                "{}".format(type(e)))
