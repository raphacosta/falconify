from Adapters.SqlAlchemy.Model.Base import Base
from Domain.Model.User import User
from Domain.Model.UserRole import UserRole
from Adapters.Repository.RoleRepository import \
    RoleRepository
from sqlalchemy.exc import DataError
from Application.Infrastructure.Exception.RuntimeException \
    import InvalidParameterError
from Application.Infrastructure.Exception.RuntimeException \
    import GeneralExceptionError
import uuid
from werkzeug.security import generate_password_hash


class UserRepository(Base):

    def __init__(self, session):
        """

        :param session:
        :type session:
        """
        self.session = session

    def list(self):
        """

        :return:
        :rtype:
        """
        try:
            return self.session.query(User).all()
        except Exception as e:
            raise GeneralExceptionError(
                "{}".format(type(e)))

    def load_by_email(self, email):
        """

        :param email:
        :type email:
        :return:
        :rtype:
        """
        try:
            return self.session.query(User)\
                .filter(User.email == email.lower())\
                .first()

        except DataError as e:
            raise InvalidParameterError(str(e.orig))
        except Exception as e:
            raise GeneralExceptionError(
                "{} - for {}".format(type(e), email))

    def load_by_uuid(self, uuid):
        """

        :param uuid:
        :type uuid:
        :return:
        :rtype:
        """
        try:
            return self.session.query(User) \
                .filter(User.uuid == uuid)\
                .first()

        except DataError as e:
            raise InvalidParameterError(str(e.orig))
        except Exception as e:
            raise GeneralExceptionError(
                "{} - for {}".format(type(e), uuid))

    def create(self, data):
        """

        :param data:
        :type data:
        :return:
        :rtype:
        """
        user = User()
        user.user_role = UserRole()

        try:
            user.uuid = uuid.uuid4()
            user.username = data['username']
            user.first_name = data['first_name']
            user.last_name = data['last_name']
            user.email = data['email'].lower()

            if 'role' in data:
                role = RoleRepository(self.session). \
                    load_by_shortname(data['role'])
            else:
                role = RoleRepository(self.session).load_default()

            if role is None:
                raise GeneralExceptionError(
                    "Default role not defined.")

            user.user_role.role_id = role.id

            if 'info' in data:
                user.info = data['info']
            user.password = generate_password_hash(
                data['password'],
                method='sha256'
            )

            self.session.add(user)
            self.session.commit()

            return user.uuid
        except Exception as e:
            raise GeneralExceptionError(
                "{}".format(type(e)))

    def update(self, uuid, data):   # noqa
        """

        :param uuid:
        :param data:
        :return:
        :rtype:
        """
        user = self.load_by_uuid(uuid)

        try:
            if 'username' in data:
                user.username = data['username']

            if 'first_name' in data:
                user.first_name = data['first_name']

            if 'last_name' in data:
                user.last_name = data['last_name']

            if 'info' in data:
                user.info = data['info']

            if 'email' in data:
                user.email = data['email'].lower()

            if 'role' in data:
                role = RoleRepository(self.session).\
                    load_by_shortname(data['role'])
            else:
                role = RoleRepository(self.session).load_default()

            if role is None:
                raise GeneralExceptionError(
                    "Default role not defined.")

            user.user_role.role_id = role.id

            if 'password' in data:
                user.password = generate_password_hash(
                    data['password'],
                    method='sha256'
                )

            self.session.commit()

            return user.uuid
        except Exception as e:
            raise GeneralExceptionError(
                "{}".format(type(e)))

    def count_users_with_email(self, email):
        """

        :param email:
        :type email:
        :return:
        :rtype:
        """
        try:
            return self.session.query(User)\
                .filter(User.email == email.lower())\
                .count()
        except Exception as e:
            raise GeneralExceptionError(
                "{}".format(type(e)))

    def retrieve_internal_id_for_user(self, uuid):
        """

        :param uuid:
        :return:
        """
        try:
            user = self.load_by_uuid(uuid)

            if not user:
                raise InvalidParameterError('User Not Found %s' % uuid)

            return user.id
        except DataError as e:
            raise InvalidParameterError(str(e.orig))
        except Exception as e:
            raise GeneralExceptionError(
                "{} - for {}".format(str(e.__dict__), uuid))
