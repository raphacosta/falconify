from Application.Infrastructure.Exception.RuntimeException \
    import GeneralExceptionError


class AccessRepository:

    def __init__(self, session):
        """

        :param session:
        :type session:
        """
        self.session = session

    def store_grant(self, user, refresh_token):
        """

        :param refresh_token:
        :param user:
        :type user:
        :return:
        :rtype:
        """
        try:
            user.last_login = 'now()'
            user.refresh_token = refresh_token
            self.session.commit()
        except Exception as e:
            raise GeneralExceptionError(
                "{}".format(type(e)))
