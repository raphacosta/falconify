import socketio
from Config import Config, Log

log = Log.get_logger()

sio = socketio.Client()
is_sio_connected = False


@sio.event
def connect():
    global is_sio_connected
    is_sio_connected = True
    log.debug('connection established')


@sio.event
def disconnect():
    global is_sio_connected
    is_sio_connected = False
    log.debug('disconnected from server')


class SocketIo:
    def __init__(self):
        self.connect()

    @staticmethod
    def connect():
        if not is_sio_connected:
            sio.connect(Config.SOCKET_IO_URL)

    @staticmethod
    def broadcast(room, data):
        data = {"data": data}
        sio.emit(room, data)

    def __del__(self):
        log.debug('Destructor called, Disconnecting.')
