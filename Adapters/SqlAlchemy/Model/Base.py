# -*- coding: utf-8 -*-
from sqlalchemy import Column
from sqlalchemy import DateTime, Integer, Boolean, func
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from Adapters.SqlAlchemy import Alchemy
import uuid


class BaseModel(object):

    id = Column(Integer(), primary_key=True)
    is_active = Column(Boolean(), default=True)
    uuid = Column(UUID(as_uuid=True), unique=True, nullable=False,
                  default=uuid.uuid4)
    created = Column(DateTime, default=func.now())
    modified = Column(DateTime, default=func.now(), onupdate=func.now())

    @declared_attr
    def __tablename__(self):
        """

        :return:
        :rtype:
        """
        return self.__name__.lower()

    @classmethod
    def find_one(cls, session, id):
        """

        :param session:
        :type session:
        :param id:
        :type id:
        :return:
        :rtype:
        """
        return session.query(cls).filter('id' == id).one()

    @classmethod
    def find_update(cls, session, id, args):
        """

        :param session:
        :type session:
        :param id:
        :type id:
        :param args:
        :type args:
        :return:
        :rtype:
        """
        return session.query(cls).filter('id' == id).\
            update(args, synchronize_session=False)

    @property
    def get_id(self):
        return self.id

    @property
    def get_uuid(self):
        return self.uuid

    @property
    def _is_active(self):
        return self.is_active

    @_is_active.setter
    def _is_active(self, status):
        self.is_active = status

    @property
    def get_stats(self):
        return {
            'created': self.created,
            'modified': self.modified

        }

    def to_dict(self):
        """

        :return:
        :rtype:
        """
        intersection = set(self.__table__.columns.keys()) & set(self.FIELDS)
        return dict(map(
            lambda key:
                (key,
                    (lambda value: self.FIELDS[key](value) if value else None)
                    (getattr(self, key))),
                intersection))

    def tuple_to_dict(self, tuple):
        """

        :param tuple:
        :type tuple:
        :return:
        :rtype:
        """
        d, a = {}, []
        for rowproxy in tuple:
            for tup in rowproxy.items():
                d = {**d, **{tup[0]: tup[1]}}  # noqa
            a.append(d)
        return a

    FIELDS = {
        'uuid': str,
        'is_active': bool,
        'created': Alchemy.datetime_to_timestamp,
        'modified': Alchemy.datetime_to_timestamp
    }


Base = declarative_base(cls=BaseModel)
