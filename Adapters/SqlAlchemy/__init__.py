# -*- coding: utf-8 -*-
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from Config import Log
from Config import Config

LOG = Log.get_logger()


def get_engine(uri):
    LOG.info('Connecting to database..')
    options = {
        'pool_recycle': 3600,
        'pool_size': 10,
        'pool_timeout': 30,
        'max_overflow': 30,
        'echo': Config.DB_ECHO,
        'execution_options': {
            'autocommit': Config.DB_AUTOCOMMIT
        }
    }
    return create_engine(uri, **options)


db_session = scoped_session(sessionmaker())
engine = get_engine(Config.DATABASE_URL)


def init_session():
    db_session.configure(bind=engine)

    # removing the create all, so we enforce the use of migrations
    # from .Model.Base import Base
    # Base.metadata.create_all(engine)
