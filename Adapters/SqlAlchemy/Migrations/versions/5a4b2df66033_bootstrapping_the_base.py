"""bootstrapping the base

Revision ID: 5a4b2df66033
Revises:
Create Date: 2019-12-11 18:52:46.576320

"""
from alembic import op
import sqlalchemy as sa
import uuid
import json
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.dialects.postgresql import JSONB
from werkzeug.security import generate_password_hash


# revision identifiers, used by Alembic.
revision = '5a4b2df66033'
down_revision = None
branch_labels = None
depends_on = None
_DEFAULT_ADMIN_USER = 'admin_user'


def upgrade():
    user = op.create_table('user',
                           sa.Column('first_name',
                                     sa.String(length=200),
                                     nullable=False),
                           sa.Column('last_name',
                                     sa.String(length=200),
                                     nullable=False),
                           sa.Column('username',
                                     sa.String(length=200),
                                     nullable=True),
                           sa.Column('email',
                                     sa.String(length=320),
                                     nullable=False),
                           sa.Column('password',
                                     sa.String(length=80),
                                     nullable=False),
                           sa.Column('id',
                                     sa.Integer(),
                                     primary_key=True,
                                     autoincrement=True),
                           sa.Column('info',
                                     JSONB,
                                     nullable=True),
                           sa.Column('last_login',
                                     sa.DateTime(),
                                     nullable=True),
                           sa.Column('refresh_token',
                                     sa.String(length=36),
                                     nullable=True),
                           sa.Column('reset_request',
                                     sa.String(length=36),
                                     nullable=True),
                           sa.Column('uuid',
                                     UUID(as_uuid=True),
                                     unique=True,
                                     nullable=False,
                                     default=uuid.uuid4()),
                           sa.Column('is_active',
                                     sa.Boolean(),
                                     index=True,
                                     server_default=sa.sql.expression.true()),
                           sa.Column('created',
                                     sa.DateTime(),
                                     default=sa.func.now()),
                           sa.Column('modified',
                                     sa.DateTime(),
                                     default=sa.func.now(),
                                     onupdate=sa.func.now()))

    # seeding the feed_sources table
    with open('/code/Adapters/SqlAlchemy/Schema/Seed/users.json', 'r') as f:
        users = json.load(f)

        for item in users:
            unique_id = uuid.uuid4()
            op.bulk_insert(user, [
                {
                    'uuid': unique_id,
                    'first_name': item['first_name'],
                    'last_name': item['last_name'],
                    'email': item['email'],
                    'username': item['username'],
                    'password': generate_password_hash(
                        item['password'],
                        method='sha256'
                    ),
                }
            ])

    role = op.create_table('role',
                           sa.Column('id',
                                     sa.Integer(),
                                     primary_key=True),
                           sa.Column('uuid',
                                     UUID(as_uuid=True),
                                     unique=True,
                                     nullable=False,
                                     index=True,
                                     default=uuid.uuid4()),
                           sa.Column('name',
                                     sa.String(length=30),
                                     nullable=False,
                                     index=True),
                           sa.Column('shortname',
                                     sa.String(length=30),
                                     nullable=False,
                                     index=True),
                           sa.Column('matrix',
                                     JSONB,
                                     nullable=True),
                           sa.Column('is_admin',
                                     sa.Boolean(),
                                     server_default=sa.sql.expression.false()),
                           sa.Column('is_custom',
                                     sa.Boolean(),
                                     server_default=sa.sql.expression.false()),
                           sa.Column('is_default',
                                     sa.Boolean(),
                                     server_default=sa.sql.expression.false()),
                           sa.Column('is_active',
                                     sa.Boolean(),
                                     server_default=sa.sql.expression.true()),
                           sa.Column('created',
                                     sa.DateTime(),
                                     default=sa.func.now()),
                           sa.Column('modified',
                                     sa.DateTime(),
                                     default=sa.func.now(),
                                     onupdate=sa.func.now()))
    # hydrate roles table
    with open('/code/Adapters/SqlAlchemy/Schema/Seed/roles.json', 'r') as f:
        role_dict = json.load(f)

        for item in role_dict:
            unique_id = uuid.uuid4()
            op.bulk_insert(role, [
                {
                    'uuid': unique_id,
                    'name': item['name'],
                    'shortname': item['shortname'],
                    'is_admin': item['is_admin'],
                    'is_default': item['is_default'],
                    'matrix': item['matrix']
                }
            ])

        # creates the user x role table
        op.create_table('user_role',
                        sa.Column('id',
                                  sa.Integer(),
                                  primary_key=True),
                        sa.Column('user_id',
                                  sa.Integer(),
                                  sa.ForeignKey('user.id'),
                                  nullable=False,
                                  index=True),
                        sa.Column('role_id',
                                  sa.Integer(),
                                  sa.ForeignKey('role.id'),
                                  nullable=False,
                                  index=True),
                        sa.Column('uuid',
                                  UUID(as_uuid=True),
                                  unique=True,
                                  nullable=False,
                                  index=True,
                                  default=uuid.uuid4()),
                        sa.Column('is_active',
                                  sa.Boolean(),
                                  server_default=sa.sql.expression.true()),
                        sa.Column('created',
                                  sa.DateTime(),
                                  default=sa.func.now()),
                        sa.Column('modified',
                                  sa.DateTime(),
                                  default=sa.func.now(),
                                  onupdate=sa.func.now()),
                        )

        # now that the roles and the user_role has been created,
        # set the role "is_admin" to the admin_user user
        _resolve_admin_role(_DEFAULT_ADMIN_USER)

    log_type = op.create_table('log_type',
                               sa.Column('name',
                                         sa.String(length=200),
                                         nullable=False),
                               sa.Column('description',
                                         sa.String(length=320),
                                         nullable=True),
                               sa.Column('id',
                                         sa.Integer(),
                                         primary_key=True),
                               sa.Column('uuid',
                                         UUID(as_uuid=True),
                                         unique=True,
                                         nullable=False,
                                         default=uuid.uuid4()),
                               sa.Column('is_active',
                                         sa.Boolean(),
                                         index=True),
                               sa.Column('created',
                                         sa.DateTime(),
                                         default=sa.func.now()),
                               sa.Column('modified',
                                         sa.DateTime(),
                                         default=sa.func.now(),
                                         onupdate=sa.func.now()))

    # hydrate log types table
    with open('/code/Adapters/SqlAlchemy/Schema/Seed/log_types.json',
              'r') as f:
        log_type_dict = json.load(f)

        for item in log_type_dict:
            unique_id = uuid.uuid4()
            op.bulk_insert(log_type, [
                {
                    'uuid': unique_id,
                    'name': item['name'],
                    'description': item['description']
                }
            ])

    op.create_table('log',
                    sa.Column('log_type_id',
                              sa.Integer(),
                              sa.ForeignKey('log_type.id'),
                              nullable=False,
                              index=True),
                    sa.Column('id',
                              sa.Integer(),
                              primary_key=True),
                    sa.Column('log',
                              JSONB,
                              nullable=False),
                    sa.Column('uuid',
                              UUID(as_uuid=True),
                              unique=True,
                              nullable=False,
                              default=uuid.uuid4()),
                    sa.Column('is_active',
                              sa.Boolean(),
                              index=True),
                    sa.Column('created',
                              sa.DateTime(),
                              default=sa.func.now()),
                    sa.Column('modified',
                              sa.DateTime(),
                              default=sa.func.now(),
                              onupdate=sa.func.now())
                    )


def _resolve_admin_role(shortname):
    """
    Resolve the admin role to the admin user previously loaded

    :param shortname:
    :return:
    """
    op.execute('INSERT INTO user_role (uuid, user_id, role_id) '
               'SELECT \'' + str(uuid.uuid4()) + '\' as uuid, '
               'u.id as user_id, r.id as role_id from "user" u '
               'left join "role" r on 1 = 1 where '
               'u.username = \'' + shortname + '\' and '
               'r.is_admin is true')


def downgrade():
    op.drop_table('user_role')
    op.drop_table('role')
    op.drop_table('user')
    op.drop_table('log')
    op.drop_table('log_type')
