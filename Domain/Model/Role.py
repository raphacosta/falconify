from sqlalchemy import Column, String, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB
from Adapters.SqlAlchemy.Model.Base import Base


class Role(Base):
    __tablename__ = 'role'

    name = Column(String(30), nullable=False)
    shortname = Column(String(30), nullable=False)
    is_admin = Column(Boolean(), default=False)
    is_default = Column(Boolean(), default=False)
    is_custom = Column(Boolean(), default=False)
    matrix = Column(JSONB, nullable=True)

    user_role = relationship("UserRole", uselist=False, back_populates="role")

    def __call__(self, *args, **kwargs):
        return "<Role(id='%s', " \
               "name='%s', " \
               "shortname='%s', " \
               "is_admin='%s', " \
               "is_custom='%s', " \
               "matrix='%s')>" % \
               (
                   self.id,
                   self.name,
                   self.shortname,
                   self.is_admin,
                   self.is_custom,
                   self.matrix
               )

    FIELDS = {
        'name': str,
        'shortname': str,
        'is_admin': int,
        'is_custom': int
    }
