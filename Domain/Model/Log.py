from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from Adapters.SqlAlchemy.Model.Base import Base
from Adapters.SqlAlchemy import Alchemy


class Log(Base):
    log_type_id = Column(
        Integer,
        ForeignKey('log_type.id'),
        nullable=False,
        index=True)
    log = Column(
        JSONB,
        nullable=True)
    log_type = relationship('LogType', back_populates='log')

    def __call__(self, *args, **kwargs):
        return "<Log(" \
               "log_type='%s', " \
               "log='%s')>" % \
               (
                   self.log_type,
                   self.log
               )

    @property
    def get_log_type(self):
        return self.log_type

    @property
    def get_log(self):
        return self.log

    FIELDS = {
        'log': Alchemy.passby
    }
