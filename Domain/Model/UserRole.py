from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.orm import relationship
from Adapters.SqlAlchemy.Model.Base import Base


class UserRole(Base):
    __tablename__ = 'user_role'

    user_id = Column(
        Integer,
        ForeignKey('user.id'),
        nullable=True,
        index=True)

    role_id = Column(
        Integer,
        ForeignKey('role.id'),
        nullable=True,
        index=True)

    user = relationship("User", back_populates="user_role")
    role = relationship("Role", back_populates="user_role")

    def __call__(self, *args, **kwargs):
        return "<Role(id='%s', " \
               "user='%s', " \
               "role='%s')>" % \
               (
                   self.id,
                   self.user,
                   self.role
               )

    FIELDS = {
        'id': int,
        'role_id': int,
        'user_id': int
    }
