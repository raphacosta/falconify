from sqlalchemy import Column, String, DateTime
from Adapters.SqlAlchemy.Model.Base import Base
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.dialects.postgresql import UUID
from Adapters.SqlAlchemy import Alchemy
from sqlalchemy.orm import relationship
import uuid


class User(Base):
    __tablename__ = 'user'

    username = Column(String(200), nullable=False)
    first_name = Column(String(200), nullable=True)
    last_name = Column(String(200), nullable=True)
    email = Column(String(320), unique=True, nullable=False)
    password = Column(String(80), nullable=False)
    info = Column(JSONB, nullable=True)
    last_login = Column(DateTime)
    reset_request = Column(
        UUID(as_uuid=True),
        unique=True,
        nullable=False,
        default=uuid.uuid4)
    refresh_token = Column(
        UUID(as_uuid=True),
        unique=True,
        nullable=False,
        default=uuid.uuid4)
    user_role = relationship("UserRole", uselist=False, back_populates="user")

    def __call__(self, *args, **kwargs):
        return "<User(id='%s', " \
               "uuid='%s', " \
               "username='%s', " \
               "first_name='%s', " \
               "last_name='%s', " \
               "email='%s', " \
               "info='%s', " \
               "last_login='%s')>" % \
               (
                   self.id,
                   self.uuid,
                   self.username,
                   self.first_name,
                   self.last_name,
                   self.email,
                   self.info,
                   self.last_login
               )

    @property
    def get_first_name(self):
        return self.first_name

    @property
    def get_last_name(self):
        return self.last_name

    @property
    def get_username(self):
        return self.username

    @property
    def get_email(self):
        return self.email

    @property
    def get_info(self):
        return self.info

    @property
    def get_last_login(self):
        return self.last_login

    FIELDS = {
        'uuid': str,
        'username': str,
        'first_name': str,
        'last_name': str,
        'email': str,
        'info': Alchemy.passby,
        'last_login': Alchemy.passby
    }
