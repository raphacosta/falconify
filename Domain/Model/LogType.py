from sqlalchemy import Column, String
from sqlalchemy.orm import relationship
from Adapters.SqlAlchemy.Model.Base import Base


class LogType(Base):
    __tablename__ = 'log_type'
    name = Column(String(200), nullable=False)
    description = Column(String(320), unique=False, nullable=True)
    log = relationship('Log', back_populates='log_type')

    def __call__(self, *args, **kwargs):
        return "<LogType(" \
               "uuid='%s', " \
               "name='%s', " \
               "description='%s')>" % \
               (
                   self.uuid,
                   self.name,
                   self.description
               )

    @property
    def get_name(self):
        return self.name

    @property
    def get_description(self):
        return self.description

    FIELDS = {
        'name': str,
        'description': str
    }
