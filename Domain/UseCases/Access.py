from Adapters.Repository.AccessRepository import AccessRepository
from Adapters.Repository.UserRepository import UserRepository
from Application.Infrastructure.Exception.RuntimeException \
    import UnauthorizedError, GeneralExceptionError, InvalidParameterError
from werkzeug.security import check_password_hash
import datetime
import jwt
from uuid import uuid4
from jwt.exceptions import \
    InvalidSignatureError, ExpiredSignatureError, DecodeError
from Config.Config import SECRET_KEY, AUTH_TIME


class AccessDomain:

    _VERSION_PREFIX = "/v1/"

    def __init__(self, session):
        """

        :param session:
        :type session:
        """
        self.session = session

    @staticmethod
    def retrieve_valid_schema(type):
        FIELDS = {
            'password': {
                'type': 'string',
                'regex': '[0-9a-zA-Z]\\w{3,14}',
                'required': True,
                'minlength': 5,
                'maxlength': 64
            },
            'email': {
                'type': 'string',
                'regex': '[a-zA-Z0-9._-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,4}',
                'required': True,
                'maxlength': 320
            },
            'uuid': {
                'type': 'string',
                'required': True,
                'maxlength': 36
            },
            'token': {
                'type': 'string',
                'required': True,
                'maxlength': 200
            },
            'refresh_token': {
                'type': 'string',
                'required': True,
                'maxlength': 36
            }
        }

        valid = {}

        if type == 'grant':
            valid = {
                'email': FIELDS['email'],
                'password': FIELDS['password']
            }

        if type == 'reset':
            valid = {
                'uuid': FIELDS['uuid'],
                'token': FIELDS['token'],
                'refresh_token': FIELDS['refresh_token']
            }

        return valid

    def grant_access(self, email, password):
        """

        :param email:
        :type email:
        :param password:
        :type password:
        :return:
        :rtype:
        """
        user = self._validate_email(email)

        if self._validate_password(user.password, password):
            try:
                token_set = self._issue_token(user.uuid)
            except:  # noqa
                return GeneralExceptionError('Error issuing token.')

            try:
                AccessRepository(self.session) \
                    .store_grant(user, token_set['refresh_token'])
            except:  # noqa
                return GeneralExceptionError('Error storing grant.')

            return {
                "user": user,
                "token_set": token_set
            }

        return False

    def reset(self, uuid, token, refresh_token):
        """

        :param uuid:
        :type uuid:
        :param token:
        :type token:
        :param refresh_token:
        :type refresh_token:
        :return:
        :rtype:
        """
        try:
            decoded_token = jwt.decode(
                token,
                SECRET_KEY,
                algorithms=['HS256'],
                verify=False)
        except DecodeError:
            raise InvalidParameterError()

        if decoded_token['public_id'] != uuid:
            raise UnauthorizedError('UUID doesn\'t match signature')

        # check if the token has expired
        if not self._check_for_expired_token(decoded_token['exp']):
            raise UnauthorizedError(
                'Your token has extrapolated the allowed refreshing time. '
                'Please login again.'
            )

        user = UserRepository(self.session).load_by_uuid(uuid)

        if user.refresh_token != refresh_token:
            raise UnauthorizedError('Your refresh token '
                                    'doesn\'t match signature')

        try:
            token_set = self._issue_token(user.uuid)
        except:  # noqa
            return GeneralExceptionError('Error issuing token.')

        try:
            AccessRepository(self.session) \
                .store_grant(user, token_set['refresh_token'])
        except:  # noqa
            return GeneralExceptionError('Error storing grant.')

        return {
            "user": user,
            "token_set": token_set
        }

    def _validate_email(self, email):
        """

        :param email:
        :type email:
        :return:
        :rtype:
        """
        user = UserRepository(self.session).load_by_email(email)

        if not user:
            raise UnauthorizedError(
                'The email provided is not correct.'
            )

        return user

    def _validate_password(self, stored_password, provided_password):
        """

        :param stored_password:
        :type stored_password:
        :param provided_password:
        :type provided_password:
        :return:
        :rtype:
        """
        if not check_password_hash(stored_password, provided_password):
            raise UnauthorizedError(
                'The password provided is not correct.'
            )

        return True

    def _check_for_expired_token(self, exp):
        """

        :param exp:
        :type exp:
        :return:
        :rtype:
        """
        if datetime.datetime.fromtimestamp(
                exp) < datetime.datetime.now() and \
                datetime.datetime.fromtimestamp(
                    exp) < \
                datetime.datetime.now() - datetime.timedelta(hours=24):
            return False

        return True

    def _issue_token(self, uuid):
        """

        :param uuid:
        :type uuid:
        :return:
        :rtype:
        """
        token = jwt.encode(
            {
                'public_id': str(uuid),
                'exp': datetime.datetime.utcnow() + datetime.timedelta(
                    minutes=AUTH_TIME
                )
            },
            SECRET_KEY)

        refresh_token = uuid4()

        token_set = {
            "token": token.decode('UTF-8'),
            "refresh_token": str(refresh_token)
        }

        return token_set

    def decrypt_token(token):
        """

        :return:
        :rtype:
        """
        try:
            decoded_token = jwt.decode(
                token,
                SECRET_KEY,
                algorithms=['HS256']
            )
        except InvalidSignatureError:
            return None
        except ExpiredSignatureError:
            # initially 440, but changing the signature expiry
            # error code for ease of integration.
            return None
        except:  # noqa
            raise UnauthorizedError(
                'Your token is invalid.'
            )

        return decoded_token

    def validate_role(self, uuid, path, method):
        """

        :param uuid:
        :param path:
        :param method:
        :return:
        """
        # loads a user based on uuid
        user = UserRepository(self.session).load_by_uuid(uuid)

        try:
            # attempt to load the roles
            role = user.user_role.role

            # if it's admin, we grant access
            if role.is_admin:
                return True

            # the constant for the versions
            path = path.replace(self._VERSION_PREFIX, '')

            if path in role.matrix and method in role.matrix[path]:
                return True

            raise UnauthorizedError(
                'You don\'t have sufficient permission to '
                'perform this action.')
        except Exception:
            raise UnauthorizedError(
                'You don\'t have a permission scheme set.')
