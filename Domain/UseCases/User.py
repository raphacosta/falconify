from Adapters.Repository.UserRepository import UserRepository


class UsersDomain:
    def __init__(self, session):
        """

        :param session:
        :type session:
        """
        self.session = session

    @staticmethod
    def retrieve_valid_schema():
        FIELDS = {
            'username': {
                'type': 'string',
                'required': True,
                'minlength': 2,
                'maxlength': 200
            },
            'first_name': {
                'type': 'string',
                'required': False,
                'minlength': 2,
                'maxlength': 200
            },
            'last_name': {
                'type': 'string',
                'required': True,
                'minlength': 2,
                'maxlength': 200
            },
            'password': {
                'type': 'string',
                'regex': '[0-9a-zA-Z]\\w{3,14}',
                'required': True,
                'minlength': 5,
                'maxlength': 64
            },
            'email': {
                'type': 'string',
                'regex': '[a-zA-Z0-9._-]+@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,4}',
                'required': True,
                'maxlength': 320
            },
            'role': {
                'type': 'string',
                'required': False,
                'minlength': 2,
                'maxlength': 20
            },
            'info': {
                'type': 'dict',
                'required': False
            }
        }

        return {
            'username': FIELDS['username'],
            'first_name': FIELDS['first_name'],
            'last_name': FIELDS['last_name'],
            'password': FIELDS['password'],
            'email': FIELDS['email'],
            'role': FIELDS['role'],
            'info': FIELDS['info']
        }

    def list_all(self):
        """

        :return:
        :rtype:
        """
        return \
            UserRepository(self.session).list()

    def load(self, uuid):
        """

        :param uuid:
        :type uuid:
        :return:
        :rtype:
        """
        return \
            UserRepository(self.session).load_by_uuid(uuid)

    def create(self, data):
        """

        :param data:
        :type data:
        :return:
        :rtype:
        """
        return \
            UserRepository(self.session).create(data)

    def update(self, uuid, data):
        """

        :param uuid:
        :type uuid:
        :param data:
        :type data:
        :return:
        :rtype:
        """
        return \
            UserRepository(self.session).update(uuid, data)

    def is_email_unique(self, name):
        """
        Validates uniqueness and implements idempotency
        :param name:
        :return:
        :rtype:
        """
        return UserRepository(self.session).\
            count_users_with_email(name)

    def find_by_email(self, name):
        """

        :param name:
        :type name:
        :return:
        :rtype:
        """
        return UserRepository(self.session).\
            load_by_email(name)
