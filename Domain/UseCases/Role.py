from Adapters.Repository.RoleRepository import RoleRepository


class RolesDomain:
    def __init__(self, session):
        """

        :param session:
        """
        self.session = session

    @staticmethod
    def retrieve_valid_schema():
        FIELDS = {
            'name': {
                'type': 'string',
                'required': True,
                'maxlength': 30
            },
            'shortname': {
                'type': 'string',
                'required': True,
                'maxlength': 30
            },
            'matrix': {
                'type': 'dict',
                'required': False
            },
            'is_admin': {
                'type': 'boolean',
                'required': False
            },
            'is_default': {
                'type': 'boolean',
                'required': False
            }
        }

        return {
            'name': FIELDS['name'],
            'shortname': FIELDS['shortname'],
            'matrix': FIELDS['matrix'],
            'is_admin': FIELDS['is_admin'],
            'is_default': FIELDS['is_default']
        }

    def list(self):
        """

        :return:
        """
        return \
            RoleRepository(self.session).list()

    def load(self, uuid):
        """

        :param uuid:
        :return:
        """
        return RoleRepository(self.session).\
            load_by_uuid(uuid)

    def load_by_shortname(self, shortname):
        """

        :param shortname:
        :return:
        """
        return RoleRepository(self.session).\
            load_by_shortname(shortname)

    def create(self, data):
        """

        :param data:
        :return:
        """
        return \
            RoleRepository(self.session).create(data)

    def update(self, uuid, data):
        """

        :param uuid:
        :param data:
        :return:
        """
        return \
            RoleRepository(self.session).update(uuid, data)
