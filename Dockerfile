FROM python:3.7

RUN ssh-agent

RUN apt-get update && apt-get install -qq -y \
  build-essential libpq-dev python-pip libffi-dev python-dev python3-dev --no-install-recommends

RUN pip install --upgrade setuptools

EXPOSE 8000

ADD . /code
WORKDIR /code

COPY Application/Infrastructure/Requirements/development.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--workers", "2", "Application.Main:application", "--reload"]
