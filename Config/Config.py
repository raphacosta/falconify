import os
import configparser

APP_ENV = os.environ.get('APP_ENV') or 'local'  # or 'production'
BRAND_NAME = 'Sample Service'
AUTHOR = 'Raphael Costa'
AUTHOR_EMAIL = 'raphagc85@gmail.com'
SOLUTION_URL = 'http://api.yourservice.dev'
DOCUMENTATION_LINK = 'coming soon'
SOCKET_IO_URL = os.environ['WS_URL']

SENTRY_DSN = 'https://XXXXXX@sentry.io/XXXXXX'

SECRET_KEY = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
AUTH_TIME = 120

INI_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'Env/{}.ini'.format(APP_ENV))

CONFIG = configparser.ConfigParser()
CONFIG.read(INI_FILE)

DATABASE = {
        "driver": os.environ.get('PSQL_DRIVER'),
        "user": os.environ.get('PSQL_USER'),
        "pass": os.environ.get('PSQL_PASS'),
        "host": os.environ.get('PSQL_HOST'),
        "database": os.environ.get('PSQL_DATABASE'),
}


DB_ECHO = True if CONFIG['database']['echo'] == 'yes' else False
DB_AUTOCOMMIT = True
LOG_LEVEL = CONFIG['logging']['level']


def get_database_url():
    return f"{DATABASE['driver']}://{DATABASE['user']}:{DATABASE['pass']}@{DATABASE['host']}/{DATABASE['database']}"  # noqa


DATABASE_URL = get_database_url()
